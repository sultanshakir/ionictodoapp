// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})

.controller('TodoListCtrl', function($scope, $ionicModal) {
    $scope.TodoListItems = [{
        task: "Catch Em All",
        status: "not done"
    }, {
        task: "Drink Water",
        status: "not done"
    }]

    // init the modal
    $ionicModal.fromTemplateUrl("modal.html", {
        scope: $scope,
        animation: "slide-in-up"
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // function to open modal
    $scope.openModal = function() {
        $scope.modal.show();
    };

    // function to close moddal
    $scope.closeModal = function() {
        $scope.modal.hide();
    };

    // clean up
    $scope.$on("$destroy", function() {
        $scope.modal.remove();
    })

    // function to new items
    $scope.addItem = function(data) {
        $scope.toDoListItems.push({
            task: data.newItem,
            status: "not done yet"
        });
        data.newItem = "";
        $scope.closeModal();
    }
})